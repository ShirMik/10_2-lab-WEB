# frozen_string_literal: true

require 'nokogiri'
require 'open-uri'

# main controller
class ViewerController < ApplicationController
  def format_xslt
    send_file(
      "#{Rails.root}/public/format.xslt",
      type: 'text/xslt'
    )
  end

  def xml
    inject_xslt = ViewerHelper.get_inject params
    client_xslt = ViewerHelper.get_client params
    rss_url = ViewerHelper.get_rss params

    respond_to do |format|
      format.xml do
        URI.open(rss_url) do |rss|
          xml = Nokogiri::XML(rss)
          xml_code = ':('

          if client_xslt
            pi = Nokogiri::XML::ProcessingInstruction.new(xml, 'xml-stylesheet',
                                                          'type="text/xsl" href="/public/format.xslt"')
            xml.root.add_previous_sibling pi
          end
          xml_code = xml

          render inline: xml_code
        end
      end
    end
  end

  def viewer
    server_xslt = ViewerHelper.get_server params
    rss_url = ViewerHelper.get_rss params

    html_code = ':('

    respond_to do |format|
      format.html do
        URI.open(rss_url) do |rss|
          xml = Nokogiri::XML(rss)

          if server_xslt
            xslt = Nokogiri::XSLT(File.read('public/format.xslt'))
            html_code = xslt.transform(xml).to_s
          else
            redirect_to action: 'xml',
                        xslt_type: params[:xslt_type],
                        xslt: params[:xslt],
                        url: params[:url]

            next
          end

          render inline: html_code
        end
      end
    end
  end
end
