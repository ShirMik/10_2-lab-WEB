# frozen_string_literal: true

Rails.application.routes.draw do
  get 'public/format.xslt', to: redirect('viewer/format_xslt')
  get 'viewer/query_tool'
  get 'viewer/format_xslt'
  get 'viewer/viewer'
  get 'viewer/xml', to: 'viewer#xml', format: 'xml'
  get '/', to: redirect('viewer/query_tool')
end
